# confd

![Latest PyPI vversion](https://img.shields.io/pypi/v/confd.svg)
![Travis-CI Build](https://travis-ci.org/xmonader/confd.png)


Distrbuted configurations over multiple nodes (simpler etcd)

## Usage
To make cluster aware configurations replicated/synced over multiple nodes of the cluster


## Installation
`pip3 install confd`

## Running


### Running quickstart
To start confd u need to specify if it allows http access using `ok/yes` or `no` parameter as the 1st argument
2nd argument would be the hostport
the rest are partners ports

- node 1 `python3 __init__.py ok 6000 6001 6002`
- node 2 `python3 __init__.py no 6001 6000 6002`
- node 3 `python3 __init__.py no 6002 6000 6001`

> You need at least two-nodes in cluster to be alive.


```
➜  confd git:(master) ✗ http POST localhost:7000/set color=Blue
HTTP/1.0 200 OK
Content-Length: 5
Content-Type: application/json
Date: Sat, 31 Mar 2018 15:49:00 GMT
Server: Werkzeug/0.12.2 Python/3.6.4

true

➜  confd git:(master) ✗ http localhost:7000/get/color     
HTTP/1.0 200 OK
Content-Length: 7
Content-Type: application/json
Date: Sat, 31 Mar 2018 15:49:16 GMT
Server: Werkzeug/0.12.2 Python/3.6.4

"Blue"
```



## Requirements
requires `pysyncobj` and `flask`


## License
Software is provided as is under BSD 3-Clause License

## Authors
`confd` was written by `Ahmed Youssef <xmonader@gmail.com>`_
