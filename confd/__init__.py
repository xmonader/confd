"""confd - conf on many nodes"""

from pysyncobj import SyncObj, replicated
from pysyncobj.batteries import ReplDict
from sys import argv
import time
from flask import Flask, request, jsonify

__version__ = '0.1.0'
__author__ = 'Ahmed Youssef <xmonader@gmail.com>'



class ConfD:
	def __init__(self, current_host, partners):
		self._current_host = current_host
		self._partners = partners
		self._d = ReplDict()
		self._syncobj = SyncObj(current_host, partners, consumers=[self._d])

	def __getitem__(self, k):
		return self._d.get(k, None)

	def __setitem__(self, k, v):
		self._d[k] = v

	def __str__(self):
		return "<@{} with [{}] >: {}".format(self._current_host, self._partners, self._d)

def start_cli():
	hostfmt = "localhost:%s"

	assert len(argv) > 3
	httpserver, port, partnersports = argv[1], argv[2], argv[3:]
	host = hostfmt%port
	partners = []
	for p in partnersports:
		partners.append(hostfmt%p)

	cfd = ConfD(host, partners)
	app = Flask(__name__)

	if httpserver in ['ok', 'yes']:
		@app.route("/get/<key>")
		def getkey(key):
		    return jsonify(cfd[key])


		@app.route("/set", methods=['POST'])
		def setkey():
			data = request.json
			for k,v in data.items():
				cfd[k] = v
			print(cfd)
			return	jsonify(True)

		app.run(port=int(port)+1000)
	else:
		while True:
			time.sleep(0.5)

if __name__ == "__main__":
	start_cli()