import setuptools

setuptools.setup(
    name="confd",
    version="0.1.0",
    url="https://github.com/xmonader/confd",

    author="Ahmed Youssef",
    author_email="xmonader@gmail.com",

    description="conf on many nodes",
    long_description=open('README.rst').read(),

    packages=setuptools.find_packages(),

    install_requires=['pysyncobj'],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
)
